
import {
    TextField,
    DialogActions,
    Dialog,
    DialogContent,
    DialogTitle,
    Button,
    Alert,
  } from "@mui/material";
import { ContactItemType, methods } from "../../../api/methods";
import { FC, Fragment, useEffect, useState } from "react";
import DatePickerComponent from "../../../components/date-picker";
import { Dayjs } from "dayjs";
import { validateData } from "./validation";


const EditModal: FC<{
    isOpen: boolean, 
    setIsOpen: React.Dispatch<React.SetStateAction<boolean>>
    updateContactsList: () => void,
    date: Dayjs | null
    setDate: React.Dispatch<React.SetStateAction<Dayjs | null>>
    contactData: ContactItemType
}> = ({isOpen, setIsOpen, updateContactsList, date, setDate, contactData}) => {
    const [isAlertVisible, setIsAlertVisible] = useState(false);
    const [editedData, setEditedData] = useState<ContactItemType>({
        id: "",
        name: "",
        mobilePhone: "",
        email: null,
        jobTitle: "",
        birthDate: null,
    }); 

    const [error, setError] = useState({
        phone: false,
        name: false,
        email: false
    })


    useEffect(() => {
        setEditedData(contactData);
    }, [contactData])

    const handleEdit = (e: any) => {
		const { name, value } = e.target;

        validateData(name, value, setError);

		setEditedData((prevData) => {
            return {
                ...prevData,
                [name]: value
            }
        })
	};

	const handleSave = async () => {
		try {
            if(!editedData || error.email || error.name || error.phone) {
                setIsAlertVisible(true);

                setTimeout(() => {
                    setIsAlertVisible(false);
                }, 5000)
                return;
            }

			const result = (await methods.putContact({
                ...editedData,
                birthDate: date!.format('YYYY-MM-DD')
            })).data;
			console.log(result);

			updateContactsList();
		} catch (e) {
			console.error(e);
		}

		setIsOpen(false);
	};

    const onClose = () => {
        setIsOpen(false);
        setEditedData(contactData)
        setError({
            phone: false,
            name: false,
            email: false
        })
    }
    
    return <Fragment>
        <Dialog open={isOpen} onClose={setIsOpen}>
            <DialogTitle>Edit Contact</DialogTitle>
            <DialogContent>
                <TextField
                    name="name"
                    label="Name"
                    value={editedData?.name}
                    onChange={handleEdit}
                    fullWidth
                    variant="outlined"
                    margin="normal"
                    error={error.name}
                />
                <TextField
                    name="email"
                    label="Email"
                    value={editedData?.email}
                    onChange={handleEdit}
                    fullWidth
                    variant="outlined"
                    margin="normal"
                    error={error.email}
                />
                <TextField
                    name="mobilePhone"
                    label="Mobile Phone"
                    value={editedData?.mobilePhone}
                    onChange={handleEdit}
                    fullWidth
                    variant="outlined"
                    margin="normal"
                    error={error.phone}
                />
                <TextField
                    name="jobTitle"
                    label="Job Title"
                    value={editedData?.jobTitle}
                    onChange={handleEdit}
                    fullWidth
                    variant="outlined"
                    margin="normal"
                />
                <DatePickerComponent value={date} setValue={setDate} />
            </DialogContent>
            <DialogActions>
                <Button onClick={handleSave} variant="contained" color="primary">
                    Save
                </Button>
                <Button onClick={onClose} variant="contained">
                    Cancel
                </Button>
            </DialogActions>
        </Dialog>

        <Alert severity="error" sx={{ display: isAlertVisible ? 'flex' : 'none', position: 'absolute', top: "20px", left: "20px", width: "calc(100% - 40px)", zIndex: 2000 }}>
            Data is not valid
        </Alert>
    </Fragment>
}

export default EditModal