import {
    TextField,
    DialogActions,
    Dialog,
    DialogContent,
    DialogTitle,
    Button,
    Alert,
  } from "@mui/material";
import { ContactCreateType, ContactItemType, methods } from "../../../api/methods";
import { FC, Fragment, useState } from "react";
import DatePickerComponent from "../../../components/date-picker";
import { Dayjs } from "dayjs";
import { validateData } from "./validation";

const CreateModal: FC<{
    isOpen: boolean, 
    setIsOpen: React.Dispatch<React.SetStateAction<boolean>>
    updateContactsList: () => void,
    date: Dayjs | null
    setDate: React.Dispatch<React.SetStateAction<Dayjs | null>>
}> = ({isOpen, setIsOpen, updateContactsList, date, setDate}) => {

    const [isAlertVisible, setIsAlertVisible] = useState(false);

    const [createdData, setCreatedData] = useState<ContactCreateType>({
		name: "",
		email: null,
		mobilePhone: "",
		jobTitle: null,
		birthDate: null,
	});

    const [error, setError] = useState({
        phone: false,
        name: false,
        email: false
    })

    const handleCreateModalClose = () => {
		setIsOpen(false);
		setCreatedData({
			name: "",
			email: null,
			mobilePhone: "",
			jobTitle: null,
			birthDate: null,
		});
	};

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		const { name, value } = e.target;

        validateData(name, value, setError);

		setCreatedData((prevData) => ({ ...prevData, [name]: value }));
	};
  
    const handleCreate = async () => {
		try {
            console.log(createdData);
            console.log(error);
            if(!createdData || error.email || error.name || error.phone) {

                setIsAlertVisible(true);

                setTimeout(() => {
                    setIsAlertVisible(false);
                }, 5000)
                return;
            }

            const result = (await methods.postContact({
                ...createdData,
                birthDate: date?.format('YYYY-MM-DD')
            })).data;
            console.log("lalalaalalal" + result);

            updateContactsList();

            setCreatedData({
                name: "",
                email: null,
                mobilePhone: "",
                jobTitle: null,
                birthDate: null,
            });
        } catch (e) {
            console.error(e);
        }
        updateContactsList();
        handleCreateModalClose();
	};
    
    return <Fragment>
        <Dialog open={isOpen} onClose={handleCreateModalClose}>
            <DialogTitle>Create Contact</DialogTitle>
            <DialogContent>
                <TextField
                    name="name"
                    label="Name"
                    value={createdData.name}
                    onChange={handleChange}
                    fullWidth
                    variant="outlined"
                    margin="normal"
                    error={error.name}
                />
                <TextField
                    name="email"
                    label="Email"
                    onChange={handleChange}
                    value={createdData.email}
                    fullWidth
                    variant="outlined"
                    margin="normal"
                    error={error.email}
                />
                <TextField
                    name="mobilePhone"
                    label="Mobile Phone"
                    onChange={handleChange}
                    value={createdData.mobilePhone}
                    fullWidth
                    variant="outlined"
                    margin="normal"
                    error={error.phone}
                />
                <TextField
                    name="jobTitle"
                    label="Job Title"
                    onChange={handleChange}
                    value={createdData.jobTitle}
                    fullWidth
                    variant="outlined"
                    margin="normal"
                />

                <DatePickerComponent value={date} setValue={setDate} />
                
                </DialogContent>

            <DialogActions>
                <Button onClick={handleCreate} variant="contained" color="primary">
                    Create
                </Button>
                <Button onClick={handleCreateModalClose} variant="contained">
                    Cancel
                </Button>
            </DialogActions>
        </Dialog>

        <Alert severity="error" sx={{ display: isAlertVisible ? 'flex' : 'none', position: 'absolute', top: "20px", left: "20px", width: "calc(100% - 40px)", zIndex: 2000 }}>
            Data is not valid
        </Alert>
    </Fragment>
}

export default CreateModal