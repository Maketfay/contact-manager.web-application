export const validateNumber = (number: string) => {
    const pattern = /^[+]?[0-9]{12}$/;

    return !pattern.test(number);
}

export const validateEmail = (email: string) => {
    if(!email.length) return false
    else {
        const pattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

        return !email.match(pattern);
    }
}

export const validateName = (name: string) => {
    return !name.length
}


export const validateData = (field: string, value: string, setError: React.Dispatch<React.SetStateAction<any>>) => {
    console.log(field, value)

   console.log(validateName(value))

    switch(field) {
        case 'name': {
            setError((prevError: any) => {
                return {
                    ...prevError,
                    name: validateName(value)
                }
            })
            break;
        }
        case 'email': {
            setError((prevError: any) => {
                return {
                    ...prevError,
                    email: validateEmail(value)
                }
            })
            break;
        }
        case 'mobilePhone': {
            setError((prevError: any) => {
                return {
                    ...prevError,
                    phone: validateNumber(value)
                }
            })
            break;
        }
    }
}