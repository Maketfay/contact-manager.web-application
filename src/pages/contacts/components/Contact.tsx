import React from "react"
import { ContactItemType, methods } from "../../../api/methods"
import {
    Box,
	TableCell,
    TableRow,
    IconButton,
  } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";


const Contact: React.FC<{
	contactData: ContactItemType;
	updateContactsList: () => void;
	handleEditModalOpen: () => void
	setCurrentContact: React.Dispatch<React.SetStateAction<ContactItemType>>
}> = ({ contactData, updateContactsList, handleEditModalOpen, setCurrentContact }) => {

	const handleDelete = async () => {
		try {
			const result = (await methods.deleteContact(contactData)).data;
			console.log(result);
			updateContactsList();
		} catch (e) {
			console.error(e);
		}
	}

    const handleEdit = () => {
        setCurrentContact(contactData);
        handleEditModalOpen()
    }

	return (
		<TableRow style={{ cursor: "pointer" }}>
			<TableCell>{contactData.name}</TableCell>
			<TableCell>{contactData.email}</TableCell>
			<TableCell>{contactData.mobilePhone}</TableCell>
			<TableCell>{contactData.jobTitle}</TableCell>
			<TableCell>{contactData.birthDate ? new Date(contactData.birthDate).toLocaleDateString() : ''}</TableCell>
			<TableCell> 
				<Box sx={{ marginLeft: "auto", display: "flex", alignItems: "center" }}>
					<IconButton onClick={handleEdit} color="primary">
						<EditIcon />
					</IconButton>
					<IconButton onClick={handleDelete} color="error">
						<DeleteIcon />
					</IconButton>
				</Box>
			</TableCell>
		</TableRow>
	);
};


export default Contact