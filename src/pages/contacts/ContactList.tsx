import React, {useState, useEffect} from "react"
import { ContactItemType, ContactsListType, PagedDataType, methods } from "../../api/methods"
import {
    Box,
    Table,
    TableBody,
    TableHead,
	TableCell,
    TableRow,
    IconButton,
	TableContainer,
	Pagination,
  } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import { Dayjs } from "dayjs";
import EditModal from "./components/EditModal";
import Contact from "./components/Contact";
import CreateModal from "./components/CreateModal";


const ContactList = () => {
	const [contactsList, setContactsList] = useState<ContactsListType>([]); 
	const [date, setDate] = useState<Dayjs | null>(null);
	const [isCreateModalOpen, setIsCreateModalOpen] = useState(false);

	const [isEditModalOpen, setIsEditModalOpen] = useState(false);
	const [currentContact, setCurrentContact] = useState<ContactItemType>({
		id: "",
		name: "",
		mobilePhone: "",
		email: null,
		jobTitle: "",
		birthDate: null,
	});

	const [totalItemsCount, setTotalItemsCount] = useState(0);

	const [pagedData, setPagedData] = useState<PagedDataType>({
		page : 1,
		pageSize: 10
	});

	const handleCreateModalOpen = () => {
	  	setIsCreateModalOpen(true);
	};

	const handleEditModalOpen = () => {
		setIsEditModalOpen(true);
  	};
  
	const handlePageChange = (event: React.ChangeEvent<unknown>, value: number) => {
		setPagedData((prevData) => ({ ...prevData, page: value }));
	};	

	useEffect(() => {	  
		updateContactsList();
	}, [pagedData.page, pagedData.pageSize]);

	const updateContactsList = async () => {
		try {
			const list = (await methods.getContactList(pagedData)).data;
			setContactsList(list.items);
			setTotalItemsCount(list.totalItemsCount);
			setCurrentContact(list.items[0]);
		} catch (e) {
			console.error(e);
		}
	};

	const tableColumns = [
		"Name",
		"Email",
		"Phone",
		"Job Title",
		"Birth Date",
		""
	]

	return (   
		<Box>
			<TableContainer sx={{marginTop: "50px"}}>
				<Table sx={{
					tableLayout: "fixed", 
					borderSpacing: "0px", 
					borderCollapse: "separate",
					border: "1px solid #E0E0E0",
					borderRadius: "10px",
					overflow: "hidden",

					".MuiTableRow-root::last-of-type": {
						".MuiTableCell-root": {
							border: "none",
						}	
					}
				}}>
					<TableHead>
						<TableRow sx={{backgroundColor: "#e7e7e7"}}>
							{tableColumns.map((item, index) => (
								<TableCell sx={{fontWeight: "bold", textTransform: "uppercase", padding: "10px"}} key={index}>{item}</TableCell>
							))}
						</TableRow>
					</TableHead>
					<TableBody>
						{contactsList?.map((item) => ( 
							<Contact 
								key={item.id} 
								contactData={item} 
								handleEditModalOpen={handleEditModalOpen} 
								updateContactsList={updateContactsList} 
								setCurrentContact={setCurrentContact}
							/>
						))}
					</TableBody>  
				</Table>
			</TableContainer>

			<Box sx={{ display: "flex", justifyContent: "flex-end", marginTop: 2 }}>
				<Pagination
					count={Math.ceil(totalItemsCount / pagedData.pageSize)}
					page={pagedData.page}
					onChange={handlePageChange}
					color="primary"
				/>
			</Box>

			<IconButton sx={{position: "fixed", bottom: "50px", right: "60px", backgroundColor: "#1976D2"}} color="info"  onClick={handleCreateModalOpen}>
				<AddIcon sx={{
					fontSize: "40px", 
					color: "white",
					"&:hover": {
					color: "#1976D2",
					}
				}}/>
			</IconButton>

			<EditModal 
				isOpen={isEditModalOpen} 
				setIsOpen={setIsEditModalOpen} 
				updateContactsList={updateContactsList} 
				contactData={currentContact}
				date={date}
				setDate={setDate}
			/>
			<CreateModal
				isOpen={isCreateModalOpen}
				setIsOpen={setIsCreateModalOpen}
				updateContactsList={updateContactsList}
				date={date}
				setDate={setDate}
			/>
		</Box>
	);
};

export default ContactList;
