import ContactList from './pages/contacts/ContactList';
import {Container , Box, CssBaseline}from '@mui/material';
import { HashRouter, Routes, Route } from 'react-router-dom';

const App = () => {
	return (
		<Box>
			<CssBaseline/>
			<Container>
				<HashRouter>
					<Routes>
						<Route path ='/' element = {<ContactList />}/>
					</Routes>
				</HashRouter>
			</Container>
		</Box>
	)
}

export default App;
