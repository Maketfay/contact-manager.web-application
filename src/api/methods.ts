import axios from "axios";

const api = axios.create({
    baseURL: "http://api.contact-manager.maketfay.com/",
    //baseURL: "http://localhost:43945/"
})

axios.interceptors.request.use(config => {
    return config;
  }, 
  error => {
    return Promise.reject(error);
  });

axios.interceptors.response.use(function (response) {
    return response;
  }, function (error) {
    return Promise.reject(error);
  });

export type PagedDataType = {
  page: number,
  pageSize: number
}

export type ContactItemType = {
  id: string,
  name: string,
  mobilePhone: string,
  email: string | null,
  jobTitle: string,
  birthDate: string | null | undefined
}

export type ContactCreateType = {
  name: string,
  mobilePhone: string,
  email: string | null,
  jobTitle: string | null,
  birthDate: string | null | undefined
}

export type ContactDeleteType = {
  id: string
}

export type ContactsListType = ContactItemType[]

export type ContactPagedListType = {
	items: ContactsListType,
	currentPage: number,
	pageSize: number,
	totalItemsCount: number
}

export const methods = {
    getContactList(data: PagedDataType){
      return api.get<ContactPagedListType>("/contact", {params:data})
    },
    putContact(data: ContactItemType){
      return api.put("/contact", data)
    },
    deleteContact(data: ContactDeleteType){
    	return api.delete("/contact", {data: data})
    },
    postContact(data: ContactCreateType){
		console.log(data);
		return api.post("/contact", data)
    }
}